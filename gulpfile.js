var gulp = require('gulp');
var concat = require('gulp-concat');



gulp.task('default', ['watchjs']);

gulp.task('concatjs', function(){
  return gulp.src('./www/js/**/*.js')
          .pipe(concat('my.bundle'))
          .pipe(gulp.dest('./www/js/'));
})

gulp.task('watchjs', function() {
  gulp.watch('./www/js/**/**.js', ['concatjs']);
});