(function(){

angular.module("fin").factory("contasAPI", function($http, config, $rootScope){
 
	var _getContas = function(){
		return $http.get(config.baseUrl + "conta.php?metodo=listarContasCliente&idcliente="+$rootScope.globals.currentUser.id);
	}

	var _getConta = function(id_conta){
		return $http.get(config.baseUrl + "conta.php?metodo=listarContaId&idconta="+id_conta);
	}

	var _saveConta = function(conta){
		conta.idcliente = $rootScope.globals.currentUser.id;
		return $http.post(config.baseUrl + "conta.php?metodo=cadastrarConta", conta)
	}

	var _editConta = function(conta){
		conta.idcliente = $rootScope.globals.currentUser.id;
		console.log(conta);
		return $http.post(config.baseUrl + "conta.php?metodo=alterarConta", conta);
	}

	var _deleteConta = function(id){
		conta = {};
		conta.id = id;
		conta.idcliente = $rootScope.globals.currentUser.id;
		return $http.post(config.baseUrl + "conta.php?metodo=deletarConta", conta);
	}



	return {
		getContas: _getContas, 
		saveConta: _saveConta,
		getConta: _getConta,
		editConta: _editConta,
		deleteConta: _deleteConta
	}; 

});

})();
