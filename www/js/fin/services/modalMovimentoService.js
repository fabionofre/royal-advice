(function(){

angular.module("fin").factory("modalMovService", function(tiposAPI, movimentoAPI, $rootScope){
	

	var _criarDespesa = {

		title: "Criar Despesa",
		close: function(modal){
			modal.hide();
		},
		save: function(modal, m, d){
			console.log(m);
			if(m.hasOwnProperty('conta')){
				mv = {};
				mv.idcliente = $rootScope.globals.currentUser.id;
				mv.tipo = "D";
				mv.cod_id_tipo = m.idtipo;
				mv.valor = m.valor;
				mv.data = d;
				mv.idconta = m.conta.id;
				mv.descricao = m.descricao;
			}else{
				mv = m;
				mv.data = d;
				mv.cod_id_tipo = m.idtipo;
				mv.idcliente = $rootScope.globals.currentUser.id;
				mv.tipo = "D";

				delete mv.idtipo;
			}
			movimentoAPI.saveMovimento(mv).then(function(response){
				console.log(response);
				modal.hide();
			}, function errorCallback(response){
				console.log(response);
				modal.hide();	
			});
		}

	}

	var _criarReceita = {

		title: "Criar Receita",
		close: function(modal){
			modal.hide();
		},
		save: function(modal, m, d){
			console.log("------------------------------");
			console.log(m);
			if(m.hasOwnProperty('conta')){
				mv = {};
				mv.idcliente = $rootScope.globals.currentUser.id;
				mv.tipo = "R";
				mv.cod_id_tipo = m.idtipo;
				mv.valor = m.valor;
				mv.data = d;
				mv.idconta = m.conta.id;
				mv.descricao = m.descricao;
			}else{
				mv = m;
				mv.data = d;
				mv.cod_id_tipo = m.idtipo;
				mv.idcliente = $rootScope.globals.currentUser.id;
				mv.tipo = "R";
				delete mv.idtipo;
			}
			movimentoAPI.saveMovimento(mv).then(function(response){
				console.log("responseeeeeeeeeeeeeeeeeeeeee")
				console.log(response);
				modal.hide();
			}, function errorCallback(response){
				console.log(response);
				modal.hide();	
			});
		}

	}

	var _tipoReceita = {
		
		title: "Criar Tipo de Receita",
		close: function(modal){
			modal.hide();
		},
		save: function(modal, m){
			console.log(m);
			console.log($rootScope.globals.currentUser);
			m.idcliente = $rootScope.globals.currentUser.id;
			if(m.id){
				tiposAPI.editTipoReceita(m).then(function(response){
					console.log(response);
					modal.hide();
				}, function errorCallback(response){
					console.log(response);
					modal.hide();	
				});
			}else{
				tiposAPI.saveTipoReceita(m).then(function(response){
					console.log(response);
					modal.hide();
				}, function errorCallback(response){
					console.log(response);
					modal.hide();	
				});
			}
		}

	}

	var _tipoDespesa = {

		title: "Criar Tipo de Despesa",
		close: function(modal){
			modal.hide();
		},
		save: function(modal, m){
			m.idcliente = $rootScope.globals.currentUser.id;
			if(m.id){
				tiposAPI.editTipoDespesa(m).then(function(response){
					console.log(response);
					modal.hide();
				}, function errorCallback(response){
					console.log(response);
					modal.hide();
				});
			}else{
				tiposAPI.saveTipoDespesa(m).then(function(response){
					console.log(response);
					modal.hide();
				}, function errorCallback(response){
					console.log(response);
					modal.hide();
				});
			}
		}
		
	}


	return {
		criarDespesa: _criarDespesa,
		criarReceita: _criarReceita,
		tipoDespesa: _tipoDespesa,
		tipoReceita: _tipoReceita
	};   
 
});

})();
