(function(){

angular.module("fin").factory("movimentoAPI", function($http, config, $rootScope){
	 
	var _getMovimentos = function(){
		return $http.get(config.baseUrl + "movimentacoes.php?metodo=listaMovimentacoes&idcliente="+$rootScope.globals.currentUser.id);
	}


	var _saveMovimento = function(movimento){
		return $http.post(config.baseUrl + "movimentacoes.php?metodo=cadastrarMovimento", movimento);
	} 

	return {
		getMovimentos: _getMovimentos,
		saveMovimento: _saveMovimento
	};   
 
});

})();
