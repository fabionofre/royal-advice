(function(){

angular.module("fin").factory("tiposAPI", function($http, config, $rootScope){
	
/*	var httpConfig = {
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	} 
*/
 
	var _getTiposReceita = function(){
		return $http.get(config.baseUrl + "tipoReceita.php?metodo=listarTiposReceita&idcliente="+$rootScope.globals.currentUser.id);
	}

	var _getTipoReceita = function(id_tipo_receita){
		console.log(id_tipo_receita);
		return $http.get(config.baseUrl + "tipoReceita.php?metodo=listarTipoReceitaId&idtreceita="+id_tipo_receita);
	}

	var _saveTipoReceita = function(tipoReceita){
		return $http.post(config.baseUrl + "tipoReceita.php?metodo=cadastraTReceita", tipoReceita);
	}

	var _editTipoReceita = function(tr){
		tr.idcliente = $rootScope.globals.currentUser.id;
		return $http.post(config.baseUrl + "tipoReceita.php?metodo=alterarTReceita", tr); 
	}

	var _deleteTipoReceita = function(id){
		tr = {};
		tr.id = id;
		return $http.post(config.baseUrl + "tipoReceita.php?metodo=deletarTReceita", tr);
	}

	var _getTiposDespesa = function(){
		return $http.get(config.baseUrl + "tipoDespesa.php?metodo=listarTiposDespesa&idcliente="+$rootScope.globals.currentUser.id);
	}

	var _getTipoDespesa = function(id_tipo_despesa){
		return $http.get(config.baseUrl + "tipoDespesa.php?metodo=listarTipoDespesaId&idtdespesa="+id_tipo_despesa);
	}

	var _editTipoDespesa = function(td){
		td.idcliente = $rootScope.globals.currentUser.id;
		return $http.post(config.baseUrl + "tipoDespesa.php?metodo=alterarDespesa", td);
	}

	var _saveTipoDespesa = function(tipoDespesa){
		return $http.post(config.baseUrl + "tipoDespesa.php?metodo=cadastraTDespesa", tipoDespesa);
	}

	var _deleteTipoDespesa = function(id){
		td = {};
		td.id = id;
		return $http.post(config.baseUrl + "tipoDespesa.php?metodo=deletaTDespesa", td);
	}



	return {
		getTiposReceita: _getTiposReceita, 
		saveTipoReceita: _saveTipoReceita,
		getTipoReceita: _getTipoReceita,
		deleteTipoReceita: _deleteTipoReceita,
		editTipoReceita: _editTipoReceita,
		getTiposDespesa: _getTiposDespesa,
		saveTipoDespesa: _saveTipoDespesa,
		getTipoDespesa: _getTipoDespesa,
		editTipoDespesa: _editTipoDespesa,
		deleteTipoDespesa: _deleteTipoDespesa
	}; 

});

})();
