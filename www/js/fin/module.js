(function(){

	angular.module("fin", ["fin.controllers"])

	.config(function($stateProvider, $urlRouterProvider) {
	  $stateProvider
	    .state('fin', {
	    url: '/fin',
	    abstract: true,
	    cache: false,
	    templateUrl: 'js/fin/views/menu.html',
	    controller: 'finCtrl',
	    controllerAs: 'vm'
	  })
		.state('fin.graficos', {
	      url: '/graficos',
	      views: { 
	        'menuContent': {
      			templateUrl: 'js/fin/views/dashboard.html',
  				controller: 'finDashCtrl',
  				cache: false,
				controllerAs: 'vm'
	        }
	      }
	    })
	  .state('fin.pagar', {
	      url: '/pagar',
	      views: {
	        'menuContent': {
				templateUrl: 'js/fin/views/pagar.html',
				controller: 'aPagarCtrl',
				controllerAs: 'vm'
	        }
	      },
	    })
	    .state('fin.receber', {
	      url: '/receber',
	      views: {
	        'menuContent': {
      			templateUrl: 'js/fin/views/receber.html',
  				controller: 'aReceberCtrl',
				controllerAs: 'vm'
	        }
	      }
	    })
	    .state('fin.contas', {
	      url: '/contas',
	      views: {
	        'menuContent': {
      			templateUrl: 'js/fin/views/contas.html',
  				controller: 'contasCtrl',
				controllerAs: 'vm'
	        }
	      }
	    })
	    .state('fin.tiposReceita', {
	      url: '/tiposreceita',
	      views: {
	        'menuContent': {
      			templateUrl: 'js/fin/views/tiposReceita.html',
  				controller: 'tiposReceitaCtrl',
				controllerAs: 'vm'
	        }
	      }
	    })
	    .state('fin.tiposDespesa', {
	      url: '/tiposdespesa',
	      views: {
	        'menuContent': {
      			templateUrl: 'js/fin/views/tiposDespesa.html',
  				controller: 'tiposDespesaCtrl',
				controllerAs: 'vm'
	        }
	      }
	    })
	    .state('fin.movimentacao', {
	      url: '/movimentacao',
	      views: {
	        'menuContent': {
      			templateUrl: 'js/fin/views/movimentacao.html',
  				controller: 'movimentacaoCtrl',
				controllerAs: 'vm'
	        }
	      }
	    })
	  // if none of the above states are matched, use this as the fallback
	  // $urlRouterProvider.otherwise('/fin/dashboard');
	});

})();