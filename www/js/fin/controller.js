(function(){

	angular.module("fin.controllers", [])
		.controller("finCtrl", finCtrl);

	function finCtrl($rootScope, $ionicModal, $scope){
		var vm = this;
    	$rootScope.tabFin = true;
	    $rootScope.tabContab = false;
	    $rootScope.tabDash = false;
	    $rootScope.tabConsult = false;
	    vm.openChat = openChat;
	    vm.closeChat = closeChat;
	    clearInterval($rootScope.atualizaChat);

		$ionicModal.fromTemplateUrl('js/fin/views/modal/modalChat.html', {
		    scope: $scope,
		    animation: 'slide-in-up'
	  	}).then(function(modal) {
	    	vm.modalChat = modal;
	  	});
		


		function openChat(){
			vm.modalChat.show();
		}

		function closeChat(){
			vm.modalChat.hide();
		}

	}

})(); 