 (function(){

	angular.module("fin.controllers")
		.controller("aPagarCtrl", aPagarCtrl)
		.controller("aReceberCtrl", aReceberCtrl)
		.controller("movimentacaoCtrl", movimentacaoCtrl);

	function aPagarCtrl($scope, $ionicModal, modalMovService, tiposAPI, contasAPI, movimentoAPI, $ionicLoading, ionicDatePicker){
		var vm = this;
		vm.openModal = openModal;
		vm.closeModal = closeModal;
		vm.criarDespesa = criarDespesa;
		vm.closeCriarMovimento = closeCriarDespesa;
		vm.abrirDatePicker = abrirDatePicker;


		vm.despesas = [];
		vm.movimento = {};
 
		init();

	    var datePicker = {
	      callback: function (val) { 
	      	var data = new Date(val);
	      	vm.movimento.data = zeroBefDate(data.getDate())+'/'+zeroBefDate(data.getUTCMonth() + 1)+'/'+data.getUTCFullYear();
	      	vm.data = data.getUTCFullYear()+'-'+zeroBefDate(data.getUTCMonth() + 1)+'-'+zeroBefDate(data.getDate());
	      },
	    };
		//Função coloca um 0 no prefixo da string quando o valor recebido for maior que 10
	    function zeroBefDate(val){
	    	if(val >= 10)
	    		return val;
	    		
	    	return '0'+val;
	    }

	    function abrirDatePicker(){
    		ionicDatePicker.openDatePicker(datePicker);
	    }


		function showSpinner(){
	        $ionicLoading.show({
	          template: '<ion-spinner icon="android"></ion-spinner>',
	          duration: 700
        	})
		}

		function init(){
			showSpinner();
			getDespesas();
			getContas();
			getTipos();
			$ionicModal.fromTemplateUrl('js/fin/views/modal/modalDespesas.html', {
			    scope: $scope,
			    animation: 'slide-in-up'
		  	}).then(function(modal) {
		    	vm.modalReceitas = modal;
		  	});
		  	invocaModalCriarMovimento();
		}

		function getDespesas(){
			movimentoAPI.getMovimentos(2).then(function(response){
				console.log(response.data);
				vm.despesas = response.data.filter(function(m){
					return m.tipo == 'D';
				});
			});
		}


		function getContas(){
			contasAPI.getContas(2).then(function(response){
				vm.contas = response.data;
				vm.movimento.conta = vm.contas[0];
			})
		}

		function getTipos(){
			tiposAPI.getTiposDespesa(2).success(function(data){
				console.log(data);
				if(!data.erro){
					vm.tipos = data;
					vm.movimento.tipo = vm.tipos[0];
				}
			})
		}

		function invocaModalCriarMovimento(){
			$ionicModal.fromTemplateUrl('js/fin/views/modal/modalCriarMovimento.html', {
			    scope: $scope,
			    animation: 'slide-in-up'
		  	}).then(function(modal) {
		    	vm.modalCriarMovimento = modal;
		  	});
		}

		function openModal(despesa){
			vm.modalReceitas.show();
			vm.despesa = despesa;
		}

		function closeModal() {
    		vm.modalReceitas.hide();
  		}

		function criarDespesa(){
			vm.modalMovCtrl = modalMovService.criarDespesa;
			vm.modalCriarMovimento.show();
		}

		function closeCriarDespesa(){
			vm.modalCriarMovimento.hide();
		}

	  	$scope.$on('modal.hidden', function() {
	  		showSpinner();
			getDespesas();
			getContas();
			getTipos();
  		});

	}

	function aReceberCtrl($ionicLoading, $scope, $ionicModal, modalMovService, tiposAPI, contasAPI, movimentoAPI, ionicDatePicker){
		var vm = this;
		vm.openModal = openModal;
		vm.closeModal = closeModal;
		vm.criarReceita = criarReceita;
		vm.abrirDatePicker = abrirDatePicker;

		vm.receitas = [];
		vm.movimento = {};
		
		init();

	    var datePicker = {
	      callback: function (val) { 
	      	var data = new Date(val);
	      	vm.movimento.data = zeroBefDate(data.getDate())+'/'+zeroBefDate(data.getUTCMonth() + 1)+'/'+data.getUTCFullYear();
	      	vm.data = data.getUTCFullYear()+'-'+zeroBefDate(data.getUTCMonth() + 1)+'-'+zeroBefDate(data.getDate());
	      },
	    };
		//Função coloca um 0 no prefixo da string quando o valor recebido for maior que 10
	    function zeroBefDate(val){
	    	if(val >= 10)
	    		return val;
	    		
	    	return '0'+val;
	    }

		function showSpinner(){
	        $ionicLoading.show({
	          template: '<ion-spinner icon="android"></ion-spinner>',
	          duration: 700
        	})
		}

		function init(){
			showSpinner();
			getReceitas();
			getContas();
			getTipos();
			invocarModalReceita();
			invocaModalCriarMovimento();
		}
 
		function getReceitas(){
			movimentoAPI.getMovimentos(2).then(function(response){
				console.log(response.data);
				if(response.data.length > 0){
					vm.receitas = response.data.filter(function(m){
						return m.tipo == 'R';
					}); 
				}
			});
		}

		function getContas(){
			contasAPI.getContas(2).then(function(response){
				console.log(response.data);
				vm.contas = response.data;
				vm.movimento.conta = vm.contas[0];
			})
		}

		function getTipos(){
			tiposAPI.getTiposReceita(2).success(function(data){
				console.log(data);
				if(!data.erro){
					vm.tipos = data;
					vm.movimento.tipo = vm.tipos[0];
				}
			})
		}

		function invocarModalReceita(){
			$ionicModal.fromTemplateUrl('js/fin/views/modal/modalReceitas.html', {
			    scope: $scope,
			    animation: 'slide-in-up'
		  	}).then(function(modal) {
		    	vm.modalReceitas = modal;
		  	});
		}

		function invocaModalCriarMovimento(){
			$ionicModal.fromTemplateUrl('js/fin/views/modal/modalCriarMovimento.html', {
			    scope: $scope,
			    animation: 'slide-in-up'
		  	}).then(function(modal) {
		    	vm.modalCriarMovimento = modal;
		  	});
		}

		function criarReceita(){
			vm.modalMovCtrl = modalMovService.criarReceita;
			vm.modalCriarMovimento.show();
		}

		function openModal(receita){
			vm.receita = receita;
			vm.modalReceitas.show();
		}

		function closeModal() {
    		vm.modalReceitas.hide();
  		}


	    function abrirDatePicker(){
    		ionicDatePicker.openDatePicker(datePicker);
	    }

	  	$scope.$on('modal.hidden', function() {
	  		showSpinner();
			getReceitas();
			getContas();
			getTipos();
  		});

	}

	function movimentacaoCtrl(ionicDatePicker, $ionicLoading, $scope, $ionicModal, movimentoAPI, contasAPI, tiposAPI, modalMovService){
		var vm = this;
		vm.openModal = openModal;
		vm.closeModal = closeModal;
		vm.openModalDespesa = openModalDespesa;
		vm.openModalReceita = openModalReceita;
		vm.openModalTipoDespesa = openModalTipoDespesa;
		vm.openModalTipoReceita = openModalTipoReceita;
		vm.abrirDatePicker = abrirDatePicker; 

		vm.movimentos = [];
		vm.movimento = {};

		init();

	    var datePicker = {
	      callback: function (val) { 
	      	var data = new Date(val);
	      	vm.movimento.data = zeroBefDate(data.getDate())+'/'+zeroBefDate(data.getUTCMonth() + 1)+'/'+data.getUTCFullYear();
	      	vm.data = data.getUTCFullYear()+'-'+zeroBefDate(data.getUTCMonth() + 1)+'-'+zeroBefDate(data.getDate());
	      },
	    };
		//Função coloca um 0 no prefixo da string quando o valor recebido for maior que 10
	    function zeroBefDate(val){
	    	if(val >= 10)
	    		return val;
	    		
	    	return '0'+val;
	    }

	    function abrirDatePicker(){
    		ionicDatePicker.openDatePicker(datePicker);
	    }

		function showSpinner(){
	        $ionicLoading.show({
	          template: '<ion-spinner icon="android"></ion-spinner>',
	          duration: 700
        	})
		}

		function init(){
			showSpinner();
			getMovimentos();
			getContas();
			invocarModalMovimento();
			invocarModalCriarMovimento();
			invocaModalCriarTipoMovimento();
		}

		function getMovimentos(){
			movimentoAPI.getMovimentos(2).then(function(response){
				vm.movimentos = response.data;
			})
		}

		function getContas(){
			contasAPI.getContas(2).then(function(response){
				vm.contas = response.data;
			})
		}

		function getTipos(t){
			if(t === "R"){
				tiposAPI.getTiposReceita(2).then(function(response){
					if(!response.erro){
						vm.tipos = response.data;
					}
				})
			}else{
				tiposAPI.getTiposDespesa(2).then(function(response){
					if(!response.erro){
						vm.tipos = response.data;
					}
				})
			}
		}

		function invocarModalMovimento(){
			$ionicModal.fromTemplateUrl('js/fin/views/modal/modalReceitas.html', {
			    scope: $scope,
			    animation: 'slide-in-up'
		  	}).then(function(modal) {
		    	vm.modalMovimento = modal;
		  	});
		}

		function invocarModalCriarMovimento(){
			$ionicModal.fromTemplateUrl('js/fin/views/modal/modalCriarMovimento.html', {
			    scope: $scope,
			    animation: 'slide-in-up'
		  	}).then(function(modal) {
		    	vm.modalCriarMovimento = modal;
		  	});
		}

		function invocaModalCriarTipoMovimento(){
			$ionicModal.fromTemplateUrl('js/fin/views/modal/modalCriarTipoMovimento.html', {
			    scope: $scope,
			    animation: 'slide-in-up'
		  	}).then(function(modal) {
		    	vm.modalCriarTipoMov = modal;
		  	});
		}


		function openModal(m){
			vm.receita = m;
			console.log(m);
			contasAPI.getConta(m.idconta).success(function(data){
				vm.receita.conta = data[0].nome;
				if(vm.receita.tipo == 'D'){
					tiposAPI.getTipoDespesa(m.cod_id_tipo).success(function(data){
						vm.receita.nomeTipo = data[0].nome;
					});
				}else{
					tiposAPI.getTipoReceita(m.cod_id_tipo).success(function(data){
						vm.receita.nomeTipo = data[0].nome;
					});
				}
				vm.modalMovimento.show();
			});
		}

		function openModalDespesa(){
			vm.movimento = {};
			vm.modalCriarMovimento.show();
			vm.modalMovCtrl = modalMovService.criarDespesa;
			getTipos("D");
		}

		function openModalReceita(){
			vm.movimento = {};
			vm.modalCriarMovimento.show();
			vm.modalMovCtrl = modalMovService.criarReceita;
			getTipos("R");
		}

		function openModalTipoDespesa(){
			vm.tipoMovimento = {};
			vm.modalCriarTipoMov.show();
			vm.tipoMovCtrl = modalMovService.tipoDespesa;
		}

		function openModalTipoReceita(){
			vm.tipoMovimento = {};
			vm.modalCriarTipoMov.show();
			vm.tipoMovCtrl = modalMovService.tipoReceita;
		}


		function closeModal() {
    		vm.modalMovimento.hide();
  		}

	  	$scope.$on('modal.hidden', function() {
	  		showSpinner();
			getMovimentos();
			getContas();
  		});


	}




})();