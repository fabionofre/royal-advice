 (function(){

	angular.module("fin.controllers")
		.controller("finDashCtrl", finDashCtrl);
		
	function finDashCtrl(movimentoAPI){
		var vm = this;

		var ultimos_tres_meses;
		var data_atual = new Date();
		var mes_atual = data_atual.getMonth() + 1;
		var mes3 = {
			receita: 0,
			despesa: 0
		};
		var mes2 = {
			receita: 0,
			despesa: 0
		};
		var mes1 = {
			receita: 0,
			despesa: 0
		};

	    vm.colors = ['#45b7cd', "#FF6347"];
	    vm.series = ['Receitas', 'Despesas'];

    	vm.labels = [nomeM(mes_atual - 2), nomeM(mes_atual - 1), nomeM(mes_atual)];


    	vm.options = {
			legend: { display: true, position: 'bottom'}
  		};

  		init();

		function init(){
			movimentoAPI.getMovimentos().then(function(response){
				if(response.data.length > 0){
					ultimos_tres_meses = response.data.filter(function(m){
						var mes = m.data.charAt(5) + m.data.charAt(6);
						return mes_atual == mes || (mes_atual - 1) == mes || (mes_atual - 2) == mes;
					});
					valor_mes_atual();
					valor_mes_2();
					valor_mes_1();
			    	vm.data = [
						[mes1.receita, mes2.receita, mes3.receita],//Receitas
						[mes1.despesa, mes2.despesa,  mes3.despesa]// Despesas
	    			];
					console.log(ultimos_tres_meses);
				}
			});
		}

		function valor_mes_atual(){
			ultimos_tres_meses.forEach(function(m){
				var mes = m.data.charAt(5) + m.data.charAt(6);
				if(mes_atual == mes){
					if(m.tipo == 'R')
						mes3.receita += parseFloat(m.valor);
					else
						mes3.despesa += parseFloat(m.valor);
					console.log(mes3);
				}
			});
		}

		function valor_mes_2(){
			ultimos_tres_meses.forEach(function(m){
				var mes = m.data.charAt(5) + m.data.charAt(6);
				if((mes_atual - 1) == mes){
					if(m.tipo == 'R')
						mes2.receita += parseFloat(m.valor);
					else
						mes2.despesa += parseFloat(m.valor);
					console.log(mes2);
				}
			});
			console.log(mes2);
		}
		function valor_mes_1(){
			ultimos_tres_meses.forEach(function(m){
				var mes = m.data.charAt(5) + m.data.charAt(6);
				if((mes_atual - 2) == mes){
					if(m.tipo == 'R')
						mes1.receita += parseFloat(m.valor);
					else
						mes1.despesa += parseFloat(m.valor);
					console.log(mes1);
				}
			});
			console.log(mes1);
		}

		function nomeM(numMes){
			var nomeMes;
			switch(numMes){
				case 1:
					nomeMes = "Janeiro"
					break;
				case 2:
					nomeMes = 'Fevereiro'
					break;
				case 3:
					nomeMes = 'Março'
					break;
				case 4:
					nomeMes = 'Abril'
					break;
				case 5:
					nomeMes = 'Maio'
					break;
				case 6:
					nomeMes = 'Junho'
					break;
				case 7:
					nomeMes = 'Julho'
					break;
				case 8:
					nomeMes = 'Agosto'
					break;
				case 9:
					nomeMes = 'Setembro'
					break;
				case 10:
					nomeMes = 'Outubro'
					break;
				case 11:
					nomeMes = 'Novembro'
					break;
				case 12:
					nomeMes = 'Dezembro'
					break;
			}
			return nomeMes;
		}



	}


})();
