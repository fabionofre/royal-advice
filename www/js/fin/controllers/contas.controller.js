 (function(){

	angular.module("fin.controllers")
		.controller("contasCtrl", contasCtrl);


	function contasCtrl($scope, $ionicModal, contasAPI, $ionicLoading, $rootScope){
		var vm = this;
		vm.openModal = openModal;
		vm.closeModal = closeModal;
		vm.criarConta = criarConta;
		vm.closeModalCriarConta = closeModalCriarConta;
		vm.criaEditaConta = criaEditaConta;
		vm.excluirConta = excluirConta;

		vm.rightButtons = ['<button>Teste</button>'];

		vm.contas = [];

		console.log($rootScope.globals);
		
		init();
		console.log($ionicModal);

		function getContas(){
			contasAPI.getContas().then(function(response){
				vm.contas = response.data;
				console.log(vm.contas);
			})
		}

		function showSpinner(){
	        $ionicLoading.show({
	          template: '<ion-spinner icon="android"></ion-spinner>',
	          duration: 700
        	})
		}

		function init(){
			getContas();
			showSpinner();
			$ionicModal.fromTemplateUrl('js/fin/views/modal/modalContas.html', {
			    scope: $scope,
			    animation: 'slide-in-up'
		  	}).then(function(modal) {
		    	vm.modalContas = modal;
		  	});

			$ionicModal.fromTemplateUrl('js/fin/views/modal/modalCriarConta.html', {
			    scope: $scope,
			    animation: 'slide-in-up'
		  	}).then(function(modal) {
		    	vm.modalCriarConta = modal;
		  	});


		}

		function openModal(id){
			vm.modalContas.show();
			vm.contaDetalhes = vm.contas[id];
		}

		function closeModal() {
    		vm.modalContas.hide();
  		}

  		function criarConta(id){
  			if(id != undefined){
  				vm.conta = vm.contas[id];
  				vm.editConta = true;
  			}else{
  				vm.conta = {};
  				vm.editConta = false;
  			}
			vm.modalCriarConta.show();
  		}

  		function closeModalCriarConta(){
  			vm.modalCriarConta.hide();
  		}

  		function criaEditaConta(){
	
  			if(vm.conta.id != undefined){
  				//Edita a conta
  				contasAPI.editConta(vm.conta).then(function(response){
					console.log(response);
					getContas();
  				});
  			}else{
  				//Cria a conta
  				contasAPI.saveConta(vm.conta).then(function(response){
  					console.log(response);
  					getContas();
  				});
  			}
			
  			closeModalCriarConta();
			showSpinner();
  		}

  		function excluirConta(id){
			contasAPI.deleteConta(id).then(function(response){
				console.log(response);
				getContas();
			})
			closeModalCriarConta();
			showSpinner();
  		}

	}

})();