 (function(){

	angular.module("fin.controllers")
		.controller("tiposReceitaCtrl", tiposReceitaCtrl)
		.controller("tiposDespesaCtrl", tiposDespesaCtrl);

	function tiposReceitaCtrl($rootScope, $scope, $ionicModal, tiposAPI, modalMovService, $ionicLoading){
		var vm = this;
		vm.criarTipoReceita = criarTipoReceita;
		vm.mostrarTipoReceita = mostrarTipoReceita;
		vm.deleteTipoReceita = deleteTipoReceita;

		vm.tiposReceita = [];
		
		init();
		console.log($rootScope.globals.currentUser);

		function init(){
			showSpinner();
			iniciarModal();
			getTipos();
		}

		function showSpinner(){
	        $ionicLoading.show({
	          template: '<ion-spinner icon="android"></ion-spinner>',
	          duration: 700
        	})
		}

		function iniciarModal(){
			$ionicModal.fromTemplateUrl('js/fin/views/modal/modalCriarTipoMovimento.html', {
			    scope: $scope,
			    animation: 'slide-in-up'
		  	}).then(function(modal) {
		    	vm.modalCriarTipoMov = modal;
		  	});
		}

		function getTipos(){
			tiposAPI.getTiposReceita().success(function(data){
				console.log(data);
				if(!data.erro)
					vm.tiposReceita = data;
			})
		}

		function criarTipoReceita(tr){
			vm.tipoMovCtrl = modalMovService.tipoReceita;
			if(tr && tr.hasOwnProperty('id')){
				vm.tipoMovimento = tr;
				vm.tipoMovCtrl.title = 'Editar Tipo de Receita';
			}else{
				vm.tipoMovimento = {};
			}
			vm.modalCriarTipoMov.show();
		}

		function mostrarTipoReceita(tipoReceita){
			vm.mostrarTipo = true;
			vm.tipoReceita = tipoReceita;
			vm.modalCriarTipoMov.show();
		}

  		function deleteTipoReceita(id){
			tiposAPI.deleteTipoReceita(id).then(function(response){
				console.log(response);
				getTipos();
			})
			vm.modalCriarTipoMov.hide();
			showSpinner();
  		}

	  	$scope.$on('modal.hidden', function() {
			showSpinner();
			getTipos();
  		});

	}

	function tiposDespesaCtrl($scope, $ionicModal, tiposAPI, modalMovService, $ionicLoading){
		var vm = this;
		vm.criarTipoDespesa = criarTipoDespesa;
		vm.deleteTipoDespesa = deleteTipoDespesa;

		vm.tiposDespesa = [];
		
		init();



		function init(){
			showSpinner();
			$ionicModal.fromTemplateUrl('js/fin/views/modal/modalCriarTipoMovimento.html', {
			    scope: $scope,
			    animation: 'slide-in-up'
		  	}).then(function(modal) {
		    	vm.modalCriarTipoMov = modal;
		  	});
		  	getTipos();
		}

		function showSpinner(){
	        $ionicLoading.show({
	          template: '<ion-spinner icon="android"></ion-spinner>',
	          duration: 700
        	})
		}

		function getTipos(){
			tiposAPI.getTiposDespesa(2).success(function(data){
				console.log(data);
				if(!data.erro)
					vm.tiposDespesa = data;
			})
		}

		function criarTipoDespesa(td){
			vm.tipoMovCtrl = modalMovService.tipoDespesa;
			if(td && td.hasOwnProperty('id')){
				vm.tipoMovimento = td;
				vm.tipoMovCtrl.title = 'Editar Tipo de Despesa';
			}else{
				vm.tipoMovimento = {};
			}
			vm.modalCriarTipoMov.show();
		}

  		function deleteTipoDespesa(id){
			tiposAPI.deleteTipoDespesa(id).then(function(response){
				console.log(response);
				getTipos();
			})
			vm.modalCriarTipoMov.hide();
			showSpinner();
  		}

	  	$scope.$on('modal.hidden', function() {
	  		showSpinner();
			getTipos();
  		});



	}

})(); 