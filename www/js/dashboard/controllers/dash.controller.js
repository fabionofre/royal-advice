(function(){

	angular.module("dash.controllers")
		.controller("dashboardCtrl", dashboardCtrl);

	function dashboardCtrl(){
		var vm = this;

	    vm.colors = ['#45b7cd', "#FF6347"];
	    vm.series = ['Receitas', 'Despesas'];

    	vm.labels = ['Janeiro', 'Fevereiro', 'Março'];

    	vm.data = [
			[65, -59, 120],
			[80, 20, -40]
    	];

    	vm.options = {
			legend: { display: true, position: 'bottom'}
  		};
	}

})();