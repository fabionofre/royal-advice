(function(){

	angular.module("dash.controllers", [])
		.controller("dashCtrl", dashCtrl);

		function dashCtrl($rootScope){
		    $rootScope.tabDash = true;
	    	$rootScope.tabFin = false;
		    $rootScope.tabContab = false;
		    $rootScope.tabConsult = false;
		    clearInterval($rootScope.atualizaChat);
		}

})(); 