(function(){

	angular.module("dash", ["dash.controllers", "chart.js"])
		.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
	  		$stateProvider
			    .state('dash', {
			    url: '/dash',
			   	abstract: true,
			    cache: false,
			    templateUrl: 'js/dashboard/views/menu.html',
			    controller: 'dashCtrl'
		  	})
		    .state('dash.dashboard', {
			    url: '/dashboard',
			    cache: false,
			    views: {
			    	'menuContent': {
					    templateUrl: 'js/dashboard/views/dashboard.html',
					    controller: 'dashboardCtrl',
					    controllerAs: 'vm'
				 	}
			    }
		  	})
		  	.state('dash.fatura', {
			    url: '/fatura',
			    cache: false,
			    views: {
			    	'menuContent': {
			    		templateUrl: 'js/dashboard/views/faturas.html',
			    		controller: 'faturaCtrl',
			    		controllerAs: 'vm'
			    	},
				}
		  	})
			$urlRouterProvider.otherwise('/dash/dashboard');  
		})
})();