(function () {
    'use strict';
 
    angular
        .module('starter')
        .controller('LoginController', LoginController);
 
    LoginController.$inject = ['$location', 'AuthenticationService', '$ionicModal', '$scope', 'UserService'];
    function LoginController($location, AuthenticationService, $ionicModal, $scope, UserService) {
        var vm = this;
 
        vm.login = login;
        vm.cadastrar = cadastrar;
 
        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
            $ionicModal.fromTemplateUrl('js/auth/modalCadastrar.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                vm.modalCadastrar = modal;   
            });
        })();
 
        function login() {
            vm.dataLoading = true;
            console.log("Email: "+vm.email+"   Senha: "+vm.password)
            AuthenticationService.Login(vm.email, vm.password, function (response) {
                if (response[0]) {
                    console.log(response);
                    $location.path('/');
                } else {
                    console.log(response);
                    vm.dataLoading = false;
                }
            });
        };

        function cadastrar(){
            UserService.Create(vm.cliente);
            vm.modalCadastrar.hide();
        }


        $scope.$on('modal.hidden', function() {
            vm.cliente = {};
        });

    }
 
})();