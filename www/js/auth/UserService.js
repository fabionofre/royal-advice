(function () {
    'use strict';
 
    angular
        .module('starter')
        .factory('UserService', UserService);
 
    UserService.$inject = ['$http', 'config'];
    function UserService($http, config) {
        var service = {};
 
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByemail = GetByemail;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
 
        return service;
 
        function GetAll() {
            return $http.get('/api/users').then(handleSuccess, handleError('Error getting all users'));
        }
 
        function GetById(id) {
            return $http.get('/api/users/' + id).then(handleSuccess, handleError('Error getting user by id'));
        }
 
        function GetByemail(email) {
            return $http.get('/api/users/' + email).then(handleSuccess, handleError('Error getting user by email'));
        }
 
        function Create(user) {
            if(user && user.hasOwnProperty('cnpj')){
                return $http.post(config.baseUrl + 'cliente.php?metodo=cadastrar_mei', user).then(handleSuccess, handleError('Error creating user'));
            }else{
                return $http.post(config.baseUrl + 'cliente.php?metodo=cadastrar', user).then(handleSuccess, handleError('Error creating user'));
            }
            
        }
 
        function Update(user) {
            return $http.put('/api/users/' + user.id, user).then(handleSuccess, handleError('Error updating user'));
        }
 
        function Delete(id) {
            return $http.delete('/api/users/' + id).then(handleSuccess, handleError('Error deleting user'));
        }
 
        // private functions
 
        function handleSuccess(res) {
            console.log(res);
            return res.data;
        }
 
        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }
 
})();