(function(){

	angular.module("contab.controllers", [])
		.controller("contabCtrl", contabCtrl);
		
	function contabCtrl($rootScope){
		var vm = this;
	    $rootScope.tabContab = true;
		$rootScope.tabFin = false;
	    $rootScope.tabDash = false;
	    $rootScope.tabConsult = false;
	    clearInterval($rootScope.atualizaChat);
	}

})(); 