(function(){

	angular.module("contab.controllers")
		.controller("gfipCtrl", gfipCtrl)
		.controller("gfipMesCtrl", gfipMesCtrl);

		function gfipCtrl($state){
			var vm = this;
			vm.impostoMes = impostoMes;
			vm.title = " ";
			 
			 function impostoMes(mes){
				$state.go("contab.gfipmes", {mes: mes});
			}
		}

		function gfipMesCtrl($stateParams, impostos){
			var vm = this;
			console.log(impostos);
			vm.mes = $stateParams.mes; 
		}

})();