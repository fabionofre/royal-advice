(function(){

	angular.module("contab.controllers")
		.controller("funcCtrl", funcCtrl);
 
	function funcCtrl($ionicLoading, $ionicModal, $scope, funcionariosAPI){
		var vm = this;



		vm.createFunc = createFunc;
		vm.saveFunc = saveFunc;
		vm.editarFunc = editarFunc;

		vm.showFormFunc = false;

		init();

		function init(){
			$ionicModal.fromTemplateUrl('js/contab/views/modalFuncionario.html', {
				scope: $scope,
				animation: 'slide-in-up'
			}).then(function(modal) {
				vm.modalFunc = modal;
			});
			getFuncAtivo(); 
		}

		function getFuncAtivo(){
			funcionariosAPI.getFuncionarios(2).success(function(data){
				vm.funcionarios = data.funcionario;
				console.log(data);
			})
		}


		function createFunc(){
			vm.funcionario = {};
			vm.modalFunc.show();
		}

		function editarFunc(funcionario){
			vm.funcionario = funcionario;
			vm.modalFunc.show();
		}

		function saveFunc(){
			if(!vm.funcionario.idfuncionario){
				vm.funcionario.status_funcionario = 1;
				vm.funcionario.idcliente = $rootScope.globals.currentUser.id;
		        $ionicLoading.show({
		          template: '<ion-spinner icon="android"></ion-spinner>',
		          duration: 700
		        })
				funcionariosAPI.saveFuncionario(vm.funcionario).success(function(response){
					console.log(response);
				})
			}else{
				//Editar funcionário
			}
			vm.modalFunc.hide();
		}





	};


})();