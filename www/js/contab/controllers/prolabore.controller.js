(function(){

	angular.module("contab.controllers")
		.controller("prolaboreCtrl", prolaboreCtrl)
		.controller("prolaboreMesCtrl", prolaboreMesCtrl);

		function prolaboreCtrl($state){
			var vm = this;
			vm.impostoMes = impostoMes;
			vm.title = " ";
			 
			 function impostoMes(mes){
				$state.go("contab.prolaboremes", {mes: mes});
			}
		}

		function prolaboreMesCtrl($stateParams, $ionicPopover, $scope, impostos){
			var vm = this;
			vm.mes = $stateParams.mes;
			console.log(impostos);
			$ionicPopover.fromTemplateUrl('prolabore-popover.html', {
				scope: $scope
			}).then(function(popover) {
				vm.popover = popover;
			});
	  	  	vm.openPopover = function($event) {
			    vm.popover.show($event);
		  	};
			vm.closePopover = function() {
				vm.popover.hide();
			};  

		}

})();