(function(){

	angular.module("contab.controllers")
		.controller("impostomCtrl", impostomCtrl)
		.controller("impostoMesCtrl", impostoMesCtrl);

		function impostomCtrl($state){
			var vm = this;
			vm.impostoMes = impostoMes;
			vm.title = "Impostos";
			 
			 function impostoMes(mes){
				$state.go("contab.impostomes", {mes: mes});
			}

		}

		function impostoMesCtrl($state, $stateParams, impostos, $ionicPopup, nomeMesFilter){
			var vm = this;
			vm.mes = $stateParams.mes;
			if(!impostos.data.erro){
				vm.impostos = impostos.data[0];
				vm.impostos.status_inss = parseInt(vm.impostos.status_inss);
				vm.impostos.status_das = parseInt(vm.impostos.status_das);
				vm.impostos.status_fgts = parseInt(vm.impostos.status_fgts);
			}else{
				$state.go("contab.impostom");
				vm.showAlert = function() {
					var alertPopup = $ionicPopup.alert({
				 		title: 'Sem Impostos para '+ nomeMesFilter(vm.mes),
				 		template: 'Este mês não possui impostos!'
					});
				}
				vm.showAlert();
			}
		}

})();
