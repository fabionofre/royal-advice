(function(){

	angular.module("contab", ["contab.controllers", "ui.utils.masks"])

	.config(function($stateProvider, $urlRouterProvider) {
	  $stateProvider 
	    .state('contab', {
	    url: '/contab',
	    abstract: true,
	    cache: false,
	    templateUrl: 'js/contab/views/menu.html',
	    controller: 'contabCtrl'
	  })
	  .state('contab.func', {
	    url: '/funcionario ',
	    views: {
	      'menuContent': {
	        templateUrl: 'js/contab/views/funcionario.html',
    	    controller: 'funcCtrl',
	    	controllerAs: 'vm'
	      },
	    }
	  })
	  .state('contab.impostom', {
	    url: '/impostom ',
	    views: {
	      'menuContent': {
	        templateUrl: 'js/contab/views/impostom.html',
    	    controller: 'impostomCtrl',
	    	controllerAs: 'vm'
	      },
	    }
	  })
	  .state('contab.gfip', {
	    url: '/gfip ',
	    views: {
	      'menuContent': {
	        templateUrl: 'js/contab/views/impostom.html',
    	    controller: 'gfipCtrl',
	    	controllerAs: 'vm'
	      },
	    }
	  })
	  .state('contab.gfipmes', {
	    url: '/gfipmes/{mes} ',
	    views: {
	      'menuContent': {
	        templateUrl: 'js/contab/views/gfip.html',
    	    controller: 'gfipMesCtrl',
	    	controllerAs: 'vm',
	    	resolve: {
	    		impostos: function(gfipAPI, $stateParams){
					return gfipAPI.getGfipAnoMes(2, $stateParams.mes);
	    		}
	    	}
	      },
	    }
	  })
  	  .state('contab.prolabore', {
	    url: '/prolabore',
	    views: {
	      'menuContent': {
	        templateUrl: 'js/contab/views/impostom.html',
    	    controller: 'prolaboreCtrl',
	    	controllerAs: 'vm'
	      },
	    }
	  })
	  .state('contab.prolaboremes', {
	    url: '/prolaboremes/{mes} ',
	    views: {
	      'menuContent': {
	        templateUrl: 'js/contab/views/prolabore.html',
    	    controller: 'prolaboreMesCtrl',
	    	controllerAs: 'vm',
	    	resolve: {
	    		impostos: function(prolaboreAPI, $stateParams){
					return prolaboreAPI.getProlaboreAnoMes(2, $stateParams.mes);
	    		}
	    	}
	      },
	    }
	  })
	  .state('contab.impostomes', {
	    url: '/impostomes/{mes}',
	    cache: false,
	    views: {
	      'menuContent': { 
	        templateUrl: 'js/contab/views/impostomes.html',
    	    controller: 'impostoMesCtrl',
	    	controllerAs: 'vm',
	    	resolve: {
	    		impostos: function(impostomAPI, $stateParams){
	    			console.log($stateParams);
					return impostomAPI.getImpostosAnoMes(2, $stateParams.mes);
	    		}
	    	}
	      },
	    }
	  })
	})

	.filter('nomeMes', function(){
		return function(numMes){
			var nomeMes;
			switch(numMes){
				case '1':
					nomeMes = "Janeiro"
					break;
				case '2':
					nomeMes = 'Fevereiro'
					break;
				case '3':
					nomeMes = 'Março'
					break;
				case '4':
					nomeMes = 'Abril'
					break;
				case '5':
					nomeMes = 'Maio'
					break;
				case '6':
					nomeMes = 'Junho'
					break;
				case '7':
					nomeMes = 'Julho'
					break;
				case '8':
					nomeMes = 'Agosto'
					break;
				case '9':
					nomeMes = 'Setembro'
					break;
				case '10':
					nomeMes = 'Outubro'
					break;
				case '11':
					nomeMes = 'Novembro'
					break;
				case '12':
					nomeMes = 'Dezembro'
					break;
			}
			
			return nomeMes;
		}
	});

})();