(function(){

angular.module("contab").factory("prolaboreAPI", function($http, config, $rootScope){
	 
	var httpConfig = {
		headers: {
			"Access-Control-Allow-Origin" : "*",
    		"Access-Control-Allow-Methods" : "GET,POST,PUT,DELETE,OPTIONS",
    		"Access-Control-Allow-Headers": "*"
		}
	} 
 


	var _getProlaboreAnoMes = function(mes){ 
		return $http.get(config.baseUrl + "prolabore.php?metodo=listarProLaboreMensalCliente&mes="+mes+"&ano=2017&idcliente="+$rootScope.globals.currentUser.id);
	}

	return {
		getProlaboreAnoMes: _getProlaboreAnoMes
	}; 

});

})();
