(function(){

angular.module("contab").factory("gfipAPI", function($http, config, $rootScope){
	 
	var httpConfig = {
		headers: {
			"Access-Control-Allow-Origin" : "*",
    		"Access-Control-Allow-Methods" : "GET,POST,PUT,DELETE,OPTIONS",
    		"Access-Control-Allow-Headers": "*"
		}
	} 
 


	var _getGfipAnoMes = function(mes){
		return $http.get(config.baseUrl + "gfip.php?metodo=listarDocGfipMensalCliente&mes="+mes+"&ano=2017&idcliente="+$rootScope.globals.currentUser.id);
	}

	return {
		getGfipAnoMes: _getGfipAnoMes
	}; 

});

})();
