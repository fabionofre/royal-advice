(function(){

angular.module("contab").factory("funcionariosAPI", function($http, config, $rootScope){

/*	var httpConfig = {
		headers: {
			"Content-Type": "application/json",
			"Access-Control-Allow-Methods": "GET, POST"
		}
	}
*/

	var _getFuncionarios = function(){
		return $http.get(config.baseUrl + "funcionario.php?metodo=listarFuncionarioAtivoCliente&idcliente="+$rootScope.globals.currentUser.id);
	}

	var _saveFuncionario = function(funcionario){ 
		console.log(funcionario);
		return $http.post(config.baseUrl + "funcionario.php?metodo=cadastrarFuncionario", funcionario); 
	}

	return { 
		getFuncionarios: _getFuncionarios,  
		saveFuncionario: _saveFuncionario 
	};

});

})();
