(function(){

angular.module("contab").factory("impostomAPI", function($http, config, $rootScope){
	
	var _getImpostos = function(){
		return $http.get(config.baseUrl + "impostosMensais.php?metodo=listarTodosImpostoMensalCliente&idcliente="+$rootScope.globals.currentUser.id);
	}

	var _getImpostosAnoMes = function(mes){
		return $http.get(config.baseUrl + "impostosMensais.php?metodo=listarImpostoMensalPorMesCliente&ano=2017&mes="+mes+"&idcliente="+$rootScope.globals.currentUser.id);
	}

	return {
		getImpostos: _getImpostos, 
		getImpostosAnoMes: _getImpostosAnoMes
	}; 

});

})();
