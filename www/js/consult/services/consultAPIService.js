(function(){

angular.module("consult").factory("consultAPI", function($http, config, $rootScope){
	 
	var _getConsults = function(){
		return $http.get(config.baseUrl + "ticket.php?metodo=listarSolicitacoes&idcliente="+$rootScope.globals.currentUser.id);
	}

	var _saveConsult = function(consulta){
		consulta.idcliente = $rootScope.globals.currentUser.id;
		return $http.post(config.baseUrl + "ticket.php?metodo=cadastrarSolicitacao", consulta);
	}

	var _saveMensagem = function(mensagem){
		return $http.post(config.baseUrl + "mensagem.php?metodo=cadastrarMensagem", mensagem);
	} 

	var _getMensagens = function(id){
		return $http.get(config.baseUrl + "mensagem.php?metodo=listarMensagens&idticket="+id);
	}

	return {
		getConsults: _getConsults,
		saveConsult: _saveConsult,
		getMensagens: _getMensagens,
		saveMensagem: _saveMensagem
	};   
 
});

})();
