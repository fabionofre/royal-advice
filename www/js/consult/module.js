(function(){

	angular.module("consult", ["consult.controllers"])

	.config(function($stateProvider, $urlRouterProvider) {
	  $stateProvider 
	    .state('consult', {
	    url: '/consult',
	    cache: false,
	    abstract: false,
	    templateUrl: 'js/consult/views/consultoria.html',
	    controller: 'consultCtrl',
	    controllerAs: 'vm'
	  })
	    .state('chat', {
	    url: '/chat',
	    cache: false,
	    params: {
	    	id_ticket: null
	    },
	    templateUrl: 'js/consult/views/chat.html',
	    controller: 'chatCtrl',
	    controllerAs: 'vm',
    	resolve: {
    		tickets: function(consultAPI, $stateParams){
    			console.log($stateParams);
				return consultAPI.getMensagens($stateParams.id_ticket);
    		}
    	}
	  })
	})

})();