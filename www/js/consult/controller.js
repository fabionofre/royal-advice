(function(){

	angular.module("consult.controllers", [])
		.controller("consultCtrl", consultCtrl)
		.controller("chatCtrl", chatCtrl);


	function chatCtrl(tickets, consultAPI, $rootScope){
		var vm = this;
		console.log(tickets);
		vm.mensagens = tickets.data;
		var idticket = tickets.data[0].idticket;
		vm.enviarMensagem = enviarMensagem;


		function enviarMensagem(){
			var msg = {
				idticket: idticket,
				ididentificacao: 32,
				mensagem: vm.mensagem
			}
			vm.mensagem = '';
			consultAPI.saveMensagem(msg).then(function(response){
				console.log(response);
			})
		}

		function getMensagens(){
			consultAPI.getMensagens(idticket).then(function(response){
				vm.mensagens = response.data;
				console.log(vm.mensagens);
			})
		}

		$rootScope.atualizaChat = setInterval(function(){getMensagens();}, 1000);



	}

	function consultCtrl($rootScope, $ionicModal, $scope, consultAPI, $state){

	    $rootScope.tabConsult = true;
	    $rootScope.tabContab = false;
		$rootScope.tabFin = false;
	    $rootScope.tabDash = false;
	    clearInterval($rootScope.atualizaChat);

    	var vm = this;
		var modalConsultoria;
		var id;

	    vm.createConsult = createConsult;
	    vm.closeModal = closeModal;
	    vm.consultorias = [];
	    vm.saveConsult = saveConsult;
	    vm.openChat = openChat;

		init();

		function init(){
			invocarModalConsultoria();
			getConsultorias();
		}

		function invocarModalConsultoria(){
			$ionicModal.fromTemplateUrl('js/consult/views/modal/modalNovaConsultoria.html', {
			    scope: $scope,
			    animation: 'slide-in-up'
		  	}).then(function(modal) {
		    	modalConsultoria = modal;
		  	});
		}

		function getConsultorias(){
			consultAPI.getConsults(32).then(function(response){
				console.log(response);
				vm.consultorias = response.data;
			})
		}

		function createConsult(){
			vm.consult = {};
			modalConsultoria.show();
		}

		function saveConsult(){
			consultAPI.saveConsult(vm.consult).then(function(response){
				console.log(response);
				openChat(response.data.id);
				closeModal();
				getConsultorias();
			})
		}

		function closeModal(){
			modalConsultoria.hide();
		}

		function openChat(id_ticket){
			$state.go("chat", {id_ticket: id_ticket});
		}
	}

})(); 